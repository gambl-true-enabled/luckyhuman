package com.getryin.mamneyr

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity


class LuckyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lucky)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.main_container,
                LuckyFragment()
            ).commitNow()
    }
}