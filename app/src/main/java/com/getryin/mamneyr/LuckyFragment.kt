package com.getryin.mamneyr

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.lucky_fragment.*
import kotlin.random.Random

class LuckyFragment : Fragment() {

    private var block = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lucky_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tap_container.setOnClickListener {
            if (!block) {
                block = true
                horay_img.visibility = View.GONE
                val scaleDownX = ObjectAnimator.ofFloat(clever_img, "scaleX", 0.7f)
                val scaleDownY = ObjectAnimator.ofFloat(clever_img, "scaleY", 0.7f)
                scaleDownX.duration = 1000
                scaleDownY.duration = 1000
                val scaleDown = AnimatorSet()
                scaleDown.play(scaleDownX).with(scaleDownY)
                scaleDown.start()

                val scaleUpX = ObjectAnimator.ofFloat(clever_img, "scaleX", 1.0f)
                val scaleUpY = ObjectAnimator.ofFloat(clever_img, "scaleY", 1.0f)
                scaleUpX.duration = 1000
                scaleUpY.duration = 1000
                val scaleUp = AnimatorSet()
                scaleUp.play(scaleUpX).with(scaleUpY)

                val scaleUpHorayX = ObjectAnimator.ofFloat(horay_img, "scaleX", 0.0f, 1.0f)
                val scaleUpHorayY = ObjectAnimator.ofFloat(horay_img, "scaleY", 0.0f, 1.0f)
                scaleUpHorayX.duration = 500
                scaleUpHorayY.duration = 500
                val scaleUpHoray = AnimatorSet()
                scaleUpHoray.play(scaleUpHorayX).with(scaleUpHorayY)

                Handler().postDelayed({
                    scaleUp.start()
                    Handler().postDelayed({
                        scaleDown.start()
                        Handler().postDelayed({
                            scaleUp.start()
                            Handler().postDelayed({
                                scaleDown.start()
                                Handler().postDelayed({
                                    scaleUp.start()
                                    scaleUpHoray.start()
                                    horay_img.visibility = View.VISIBLE
                                    block = false
                                }, 1000)
                            }, 1000)
                        }, 1000)
                    }, 1000)
                }, 1000)
                val random1 = Random.nextInt(0, 100)
                val random2 = Random.nextInt(0, 100)
                val random3 = Random.nextInt(0, 100)
                val random4 = Random.nextInt(0, 100)
                val random5 = Random.nextInt(0, 100)
                val random6 = Random.nextInt(0, 100)
                val animatorText = ValueAnimator.ofInt(0, random1)
                animatorText.duration = 1000
                animatorText.addUpdateListener {
                    luck_value.text = (it.animatedValue.toString() + "%")
                }
                animatorText.start()

                val animatorText1 = ValueAnimator.ofInt(random1, random2)
                animatorText1.duration = 1000
                animatorText1.addUpdateListener {
                    luck_value.text = (it.animatedValue.toString() + "%")
                }

                val animatorText2 = ValueAnimator.ofInt(random2, random3)
                animatorText2.duration = 1000
                animatorText2.addUpdateListener {
                    luck_value.text = (it.animatedValue.toString() + "%")
                }

                val animatorText3 = ValueAnimator.ofInt(random3, random4)
                animatorText3.duration = 1000
                animatorText3.addUpdateListener {
                    luck_value.text = (it.animatedValue.toString() + "%")
                }

                val animatorText4 = ValueAnimator.ofInt(random4, random5)
                animatorText4.duration = 1000
                animatorText4.addUpdateListener {
                    luck_value.text = (it.animatedValue.toString() + "%")
                }

                val animatorText5 = ValueAnimator.ofInt(random5, random6)
                animatorText5.duration = 1000
                animatorText5.addUpdateListener {
                    luck_value.text = (it.animatedValue.toString() + "%")
                }

                Handler().postDelayed({
                    animatorText1.start()
                    Handler().postDelayed({
                        animatorText2.start()
                        Handler().postDelayed({
                            animatorText3.start()
                            Handler().postDelayed({
                                animatorText4.start()
                                Handler().postDelayed({
                                    animatorText5.start()
                                }, 1000)
                            }, 1000)
                        }, 1000)
                    }, 1000)
                }, 1000)
            }
        }
    }
}